package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class CONTACTS3 extends Activity {
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.contacts3);
	        ActionBar ab = getActionBar();
	        ab.setDisplayHomeAsUpEnabled(true);
	        getActionBar().setTitle(Html.fromHtml("<b><font color='#ffffff'>BUKguide</font></b>"));
	        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#2885ff"));
	    	ab.setBackgroundDrawable(colorDrawable);
	    	Button PRESIDENT =(Button)findViewById(R.id.Button1);
	    	Button VICEPRESIDENT =(Button)findViewById(R.id.Button2);
	    	Button SECRETARYGENERAL =(Button)findViewById(R.id.Button3);
	    	Button ASSTSECRETARYGEN =(Button)findViewById(R.id.Button4);
	    	Button FINANCIALSECRETARY =(Button)findViewById(R.id.Button5);
	    	Button TREASURER =(Button)findViewById(R.id.Button6);
	    	Button WELFARE =(Button)findViewById(R.id.Button7);
	    	Button SOCIAL =(Button)findViewById(R.id.Button8);
	    	Button SALES =(Button)findViewById(R.id.Button9);
	    	Button FOOD =(Button)findViewById(R.id.Button10);
	    	Button SPORTS =(Button)findViewById(R.id.Button11);
	    	Button PRO =(Button)findViewById(R.id.Button12);
	    	Button SPEAKER =(Button)findViewById(R.id.Button13);
	    	Button PARLIAMENT =(Button)findViewById(R.id.Button14);
	    	Button DEPUTYSPEAKER =(Button)findViewById(R.id.Button15);
	    	Button DEPUTYPARL=(Button)findViewById(R.id.Button16);
	    	
	    	PRESIDENT.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	VICEPRESIDENT.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	SECRETARYGENERAL.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	ASSTSECRETARYGEN.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	FINANCIALSECRETARY.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 TREASURER.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 WELFARE.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 SOCIAL.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 SALES.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 FOOD.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 SPORTS .setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 PRO.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 SPEAKER.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 PARLIAMENT.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 DEPUTYSPEAKER.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	 DEPUTYPARL.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:07063055189"));
					startActivity(btncall);
	    		}
	    	});
	    	
	    	

	    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu); 
	}

	
 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		
		case R.id.maps:
		      // Code you want run when activity is clicked
		     Intent mintent = new Intent (this,Second.class);
		     startActivity(mintent);
		      return true;
		case R.id.Ahome:
		      // Code you want run when activity is clicked
		     Intent dintent = new Intent (this,DASHBOARDLAY.class);
		     startActivity(dintent);
		      return true;
		case R.id.newcampus:
		      // Code you want run when activity is clicked
		     Intent hintent = new Intent (this,CONTACTS1.class);
		     startActivity(hintent);
		      return true;
		case R.id.oldcampus:
		      // Code you want run when activity is clicked
		     Intent fintent = new Intent (this,CONTACTS2.class);
		     startActivity(fintent);
		      return true;
		case R.id.sug:
		      // Code you want run when activity is clicked
		     Intent pintent = new Intent (this,CONTACTS3.class);
		     startActivity(pintent);
		      return true;
		case android.R.id.home:
			this.finish();
			default:
			return super.onOptionsItemSelected(item);
		}
		
	}

}
