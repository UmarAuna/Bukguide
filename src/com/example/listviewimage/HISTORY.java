package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

@SuppressLint("NewApi")
public class HISTORY extends Activity {
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.history);
	        ImageView image=(ImageView)findViewById(R.id.imageView1);
	        ImageView image2=(ImageView)findViewById(R.id.imageView2);
	        image.setOnClickListener(new OnClickListener(){
	        	public void onClick(View v){
	        		Intent photo = new Intent(HISTORY.this,HISTORY3.class);
	        		startActivity(photo);
	        	}
	        });
	        image2.setOnClickListener(new OnClickListener(){
	        	public void onClick(View v){
	        		Intent photo = new Intent(HISTORY.this,HISTORY4.class);
	        		startActivity(photo);
	        	}
	        });


	    }
	 
}
