package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class CONTACTS2 extends Activity {
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.contacts2);
	        ActionBar ab = getActionBar();
	        ab.setDisplayHomeAsUpEnabled(true);
	        getActionBar().setTitle(Html.fromHtml("<b><font color='#ffffff'>BUKguide</font></b>"));
	        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#2885ff"));
	    	ab.setBackgroundDrawable(colorDrawable);
	    	Button Security1 =(Button)findViewById(R.id.Button011);
	    	Button Security2 =(Button)findViewById(R.id.Button022);
	    	Button Security3 =(Button)findViewById(R.id.Button033);
	    	Button Ambulance =(Button)findViewById(R.id.Button044);
	    	Button Clinic =(Button)findViewById(R.id.Button055);
	    	Button FireService =(Button)findViewById(R.id.Button066);
	    	
	    	Security1.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:08095212221"));
					startActivity(btncall);
	    		}
	    	});
	    	Security2.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:08095210641"));
					startActivity(btncall);
	    		}
	    	});
	    	Security3.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:08095210652"));
					startActivity(btncall);
	    		}
	    	});
	    	Ambulance.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:08034167320"));
					startActivity(btncall);
	    		}
	    	});
	    Clinic.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:08099493225"));
					startActivity(btncall);
	    		}
	    	});
	    	FireService.setOnClickListener(new OnClickListener(){
	    		public void onClick(View view){
	    			Intent btncall= new Intent(Intent.ACTION_CALL);
					btncall.setData(Uri.parse("tel:064895002"));
					startActivity(btncall);
	    		}
	    	});
	    	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu); 
	}

	
 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		
		case R.id.maps:
		      // Code you want run when activity is clicked
		     Intent mintent = new Intent (this,Second.class);
		     startActivity(mintent);
		      return true;
		case R.id.Ahome:
		      // Code you want run when activity is clicked
		     Intent dintent = new Intent (this,DASHBOARDLAY.class);
		     startActivity(dintent);
		      return true;
		case R.id.newcampus:
		      // Code you want run when activity is clicked
		     Intent hintent = new Intent (this,CONTACTS1.class);
		     startActivity(hintent);
		      return true;
		case R.id.oldcampus:
		      // Code you want run when activity is clicked
		     Intent fintent = new Intent (this,CONTACTS2.class);
		     startActivity(fintent);
		      return true;
		case R.id.sug:
		      // Code you want run when activity is clicked
		     Intent pintent = new Intent (this,CONTACTS3.class);
		     startActivity(pintent);
		      return true;
		case android.R.id.home:
			this.finish();
			default:
			return super.onOptionsItemSelected(item);
		}
		
	}
}
