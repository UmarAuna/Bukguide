package com.example.listviewimage;

import java.util.ArrayList;
import java.util.Arrays;



import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends ListActivity {

	private EditText et;
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu); 
	}
	
 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		
		case R.id.maps:
		      // Code you want run when activity is clicked
		     Intent mintent = new Intent (this,Second.class);
		     startActivity(mintent);
		      return true;
		case R.id.Ahome:
		      // Code you want run when activity is clicked
		     Intent dintent = new Intent (this,DASHBOARDLAY.class);
		     startActivity(dintent);
		      return true;
		case R.id.newcampus:
		      // Code you want run when activity is clicked
		     Intent hintent = new Intent (this,CONTACTS1.class);
		     startActivity(hintent);
		      return true;
		case R.id.oldcampus:
		      // Code you want run when activity is clicked
		     Intent fintent = new Intent (this,CONTACTS2.class);
		     startActivity(fintent);
		      return true;
		case R.id.sug:
		      // Code you want run when activity is clicked
		     Intent pintent = new Intent (this,CONTACTS3.class);
		     startActivity(pintent);
		      return true;
		case android.R.id.home:
			this.finish();
			default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	private String[] listview_names = {"Faculty of Agriculture",
									   "Faculty of Arts and Islamic Studies",
									   "Faculty of Computer Science and Information Technology","Faculty of Dentistry","Faculty of Education","Faculty of Engineering","Faculty of Law","Faculty of Medicine","Faculty of Science","Faculty of Social and Management Sciences"};
	
	
	private int[] listview_images   = {R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk,R.drawable.buk};
	
	private ArrayList<String> array_sort;
	private ArrayList<Integer> image_sort;
	int textlength=0;
	
	private ListView lv;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		  ActionBar ab = getActionBar();
	        ab.setDisplayHomeAsUpEnabled(true);
	        getActionBar().setTitle(Html.fromHtml("<b><font color='#ffffff'>BUKguide</font></b>"));
	        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#2885ff"));
	    	ab.setBackgroundDrawable(colorDrawable);

		et 	= 	(EditText) findViewById(R.id.EditText01);
		lv	=	(ListView) findViewById(android.R.id.list);
		
		array_sort=new ArrayList<String> (Arrays.asList(listview_names));
		image_sort=new ArrayList<Integer>();
		for (int index = 0; index < listview_images.length; index++)
	    {
			image_sort.add(listview_images[index]);
	    }
		
		setListAdapter(new bsAdapter(this));


		et.addTextChangedListener(new TextWatcher()
		{
			public void afterTextChanged(Editable s)
			{
                  // Abstract Method of TextWatcher Interface.
			}
			public void beforeTextChanged(CharSequence s,
					int start, int count, int after)
			{
				// Abstract Method of TextWatcher Interface.
			}
			public void onTextChanged(CharSequence s,
					int start, int before, int count)
			{
				textlength = et.getText().length();
				array_sort.clear();
				image_sort.clear();
				for (int i = 0; i < listview_names.length; i++)
				{
					if (textlength <= listview_names[i].length())
					{
						/***
						 * If you want to highlight the Faculties which start with 
						 * entered letters then choose this block. 
						 * And comment the below If condition Block
						 */
						/*if(et.getText().toString().equalsIgnoreCase(
								(String)
								listview_names[i].subSequence(0,
										textlength)))
						{
							array_sort.add(listview_names[i]);
							image_sort.add(listview_images[i]);
						}*/
						
						/***
						 * If you choose the below block then it will act like a 
						 * Like operator in the Mysql
						 */
						
						if(listview_names[i].toLowerCase().contains(
								et.getText().toString().toLowerCase().trim()))
						{
							array_sort.add(listview_names[i]);
							image_sort.add(listview_images[i]);
						}
                      }
				}
				AppendList(array_sort,image_sort);
				}
				});
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0,
			                    View arg1, int position, long arg3)
			{
			  
				if(array_sort.get(position).equals("Faculty of Agriculture"))
				{
					Intent i=new Intent(getApplicationContext(),AGRIC.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Arts and Islamic Studies"))
				{
					Intent i=new Intent(getApplicationContext(),FAIS.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Computer Science and Information Technology"))
				{
					Intent i=new Intent(getApplicationContext(),FCSIT.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Dentistry"))
				{
					Intent i=new Intent(getApplicationContext(),DENTISTRY.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Education"))
				{
					Intent i=new Intent(getApplicationContext(),EDUCATION.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Engineering"))
				{
					Intent i=new Intent(getApplicationContext(),ENGINEERING.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Law"))
				{
					Intent i=new Intent(getApplicationContext(),LAW.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Medicine"))
				{
					Intent i=new Intent(getApplicationContext(),MEDICINE.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Science"))
				{
					Intent i=new Intent(getApplicationContext(),SCIENCE.class);
					startActivity(i);
					
				}
				if(array_sort.get(position).equals("Faculty of Social and Management Sciences"))
				{
					Intent i=new Intent(getApplicationContext(),SMS.class);
					startActivity(i);
					
				}
				
				
			}
		});
	}
	
	public void AppendList(ArrayList<String> str,ArrayList<Integer> img)
	{
		setListAdapter(new bsAdapter(this));
	}
	
	public class bsAdapter extends BaseAdapter
    {
        Activity cntx;
        public bsAdapter(Activity context)
        {
            // TODO Auto-generated constructor stub
            this.cntx=context;

        }

        public int getCount()
        {
            // TODO Auto-generated method stub
            return array_sort.size();
        }

        public Object getItem(int position)
        {
            // TODO Auto-generated method stub
            return array_sort.get(position);
        }

        public long getItemId(int position)
        {
            // TODO Auto-generated method stub
            return array_sort.size();
        }

        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View row=null;
            
            LayoutInflater inflater=cntx.getLayoutInflater();
            row=inflater.inflate(R.layout.search_list_item, null);
            
            TextView   tv	=	(TextView)	row.findViewById(R.id.title);
            ImageView im	=	(ImageView)	row.findViewById(R.id.imageview);
            
            tv.setText(array_sort.get(position));
            im.setImageDrawable(getResources().getDrawable(image_sort.get(position)));
            
        return row;
        }
    }
}