package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class HISTORY1 extends TabActivity{
	 @SuppressWarnings("deprecation")
		@SuppressLint("NewApi")
			public void onCreate(Bundle savedInstanceState) {
			        super.onCreate(savedInstanceState);
			        setContentView(R.layout.history1);
			        ActionBar ab = getActionBar();
			        ab.setDisplayHomeAsUpEnabled(true);
			        getActionBar().setTitle(Html.fromHtml("<b><font color='#ffffff'>BUKguide</font></b>"));
			        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#2885ff"));
			    	ab.setBackgroundDrawable(colorDrawable);

					TabHost tabHost =(TabHost)findViewById(android.R.id.tabhost);
					
					
					Intent intentAbout = new Intent().setClass(this,HISTORY.class);
					TabSpec tabSpecAbout = tabHost
							.newTabSpec("HISTORY")
							.setIndicator("HISTORY")
							.setContent(intentAbout);
				
							Intent intentteam = new Intent().setClass(this, HISTORY2.class);
							TabSpec tabSpecteam = tabHost
									.newTabSpec("PAST VICE CHANCELLOR'S")
									.setIndicator("PAST VICE CHANCELLOR'S")
									.setContent(intentteam);
							
						
							
							
				 tabHost.addTab(tabSpecAbout);
				 tabHost.addTab(tabSpecteam);
				 tabHost.setCurrentTab(3);
				 
				}
			        

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			MenuInflater blowUp = getMenuInflater();
			blowUp.inflate(R.menu.main, menu);
			return super.onCreateOptionsMenu(menu); 
		}

		
	 @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()){
			
			case R.id.maps:
			      // Code you want run when activity is clicked
			     Intent mintent = new Intent (this,Second.class);
			     startActivity(mintent);
			      return true;
			case R.id.Ahome:
			      // Code you want run when activity is clicked
			     Intent dintent = new Intent (this,DASHBOARDLAY.class);
			     startActivity(dintent);
			      return true;
			case R.id.newcampus:
			      // Code you want run when activity is clicked
			     Intent hintent = new Intent (this,CONTACTS1.class);
			     startActivity(hintent);
			      return true;
			case R.id.oldcampus:
			      // Code you want run when activity is clicked
			     Intent fintent = new Intent (this,CONTACTS2.class);
			     startActivity(fintent);
			      return true;
			case R.id.sug:
			      // Code you want run when activity is clicked
			     Intent pintent = new Intent (this,CONTACTS3.class);
			     startActivity(pintent);
			      return true;
			case android.R.id.home:
				this.finish();
				default:
				return super.onOptionsItemSelected(item);
			}
			
		}
}
