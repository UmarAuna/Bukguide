package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FCSIT extends Activity {

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 try{
		setContentView(R.layout.fcsit);
		 ActionBar actionBar = getActionBar();
	        actionBar.setDisplayHomeAsUpEnabled(true);
	        getActionBar().setTitle(Html.fromHtml("<b><font color='#ffffff'>BUKguide</font></b>"));
	        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#2885ff"));
	    	actionBar.setBackgroundDrawable(colorDrawable);
    	ListView mlistView = (ListView) findViewById(R.id.listView1);
    	mlistView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,
    	new String[]{"Department of Computer Science","Department of Information Technology","Department of Software Engineering","Department of Computer Studies with Economics"}));
    	mlistView.setTextFilterEnabled(true);
    	mlistView.setOnItemClickListener(new OnItemClickListener(){
    		
    		public void onItemClick(AdapterView<?>parent, View view, int position, long id){
    			Toast.makeText(getApplicationContext(),((TextView)view).getText(),Toast.LENGTH_SHORT).show();
    			String sText=((TextView) view).getText().toString();
    			Intent intent=null;
    					if (sText.equals("Department of Computer Science"))
    						intent= new Intent (getBaseContext(),FCSIT1.class);
    					else if (sText.equals("Department of Information Technology"))
    						intent= new Intent (getBaseContext(),FCSIT2.class);
    					else if (sText.equals("Department of Software Engineering"))
    						intent= new Intent (getBaseContext(),FCSIT3.class);
    					else if (sText.equals("Department of Computer Studies with Economics"))
    						intent= new Intent (getBaseContext(),FCSIT4.class);
    					if (intent !=null) startActivity(intent);
    		}
    	});
    	
    }catch (Exception e){
    	e.printStackTrace();
    }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		MenuInflater blowUp = getMenuInflater();
		blowUp.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu); 
	}

	
 @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		
		case R.id.maps:
		      // Code you want run when activity is clicked
		     Intent mintent = new Intent (this,Second.class);
		     startActivity(mintent);
		      return true;
		case R.id.Ahome:
		      // Code you want run when activity is clicked
		     Intent dintent = new Intent (this,DASHBOARDLAY.class);
		     startActivity(dintent);
		      return true;
		case R.id.newcampus:
		      // Code you want run when activity is clicked
		     Intent hintent = new Intent (this,CONTACTS1.class);
		     startActivity(hintent);
		      return true;
		case R.id.oldcampus:
		      // Code you want run when activity is clicked
		     Intent fintent = new Intent (this,CONTACTS2.class);
		     startActivity(fintent);
		      return true;
		case R.id.sug:
		      // Code you want run when activity is clicked
		     Intent pintent = new Intent (this,CONTACTS3.class);
		     startActivity(pintent);
		      return true;
		case android.R.id.home:
			this.finish();
			default:
			return super.onOptionsItemSelected(item);
		}
		
	}

}
