package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

@SuppressLint("NewApi")
public class DASHBOARDLAY extends Activity {
	TextToSpeech ttopj;
	 @SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.dashboardlay);
	        ActionBar ab = getActionBar();
	        getActionBar().setTitle(Html.fromHtml("<b><font color='#ffffff'>BUKguide</font></b>"));
	        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#2885ff"));
	    	ab.setBackgroundDrawable(colorDrawable);
	    }
	
	 
	    /**
	     * Button click handler on Main activity
	     * @param v
	     */
	    public void onButtonClicker(View v)
	    {
	     Intent intent;
	     switch (v.getId()) {
	  case R.id.history:
	   intent = new Intent(this, HISTORY1.class);
	   startActivity(intent);
	   break;
	  case R.id.faculties:
	   intent = new Intent(this, MainActivity.class);
	   startActivity(intent);
	   break;
	  case R.id.vicechancellors:
	   intent = new Intent(this,OFFICE.class);
	   startActivity(intent);
	   break;
	  case R.id.miscellaneous:
	   intent = new Intent(this, MISCELLANEOUS.class);
	   startActivity(intent);
	   break;
	  case R.id.about:
	   intent = new Intent(this, ABOUT.class);
	   startActivity(intent);
	   break;
	  case R.id.contacts:
	   intent = new Intent(this, CONTACTS.class);
	   startActivity(intent);
	   break;
	  default:
	   break;
	  }
	    }
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// TODO Auto-generated method stub
			super.onCreateOptionsMenu(menu);
			MenuInflater blowUp = getMenuInflater();
			blowUp.inflate(R.menu.main, menu);
			return super.onCreateOptionsMenu(menu); 

		}
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()){
			case android.R.id.home:
				this.finish();
			case R.id.maps:
			      // Code you want run when activity is clicked
			     Intent mintent = new Intent (this,Second.class);
			     startActivity(mintent);
			      return true;
			case R.id.newcampus:
			      // Code you want run when activity is clicked
			     Intent hintent = new Intent (this,CONTACTS1.class);
			     startActivity(hintent);
			      return true;
			case R.id.oldcampus:
			      // Code you want run when activity is clicked
			     Intent fintent = new Intent (this,CONTACTS2.class);
			     startActivity(fintent);
			      return true;
			case R.id.sug:
			      // Code you want run when activity is clicked
			     Intent pintent = new Intent (this,CONTACTS3.class);
			     startActivity(pintent);
			      return true;
			
			    default:
			      return super.onOptionsItemSelected(item);

			}
		}
}
