package com.example.listviewimage;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class SPLASHSCREEN extends Activity{
	long Delay = 3000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splashscreen);
		Timer RunSplash = new Timer();
		TimerTask ShowSplash = new TimerTask(){
		public void run(){
			finish();
			Intent nextIntent = new Intent(SPLASHSCREEN.this,DASHBOARDLAY.class);
			startActivity(nextIntent);
		}
		};
		RunSplash.schedule(ShowSplash, Delay);
	}	
}
