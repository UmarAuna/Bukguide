package com.example.listviewimage;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.webkit.WebView;

public class MISCELLANEOUS2 extends Activity{
	WebView mwebView = null;
	
	 @SuppressLint("NewApi")
		public void onCreate(Bundle savedInstanceState) {
		        super.onCreate(savedInstanceState);
		        setContentView(R.layout.miscellaneous2);
		        mwebView=(WebView)findViewById(R.id.webView1);
				mwebView.getSettings().setBuiltInZoomControls(true);
				mwebView.loadUrl("http://www.buk.edu.ng/?q=2014_bulletin");
		       
		}

}
